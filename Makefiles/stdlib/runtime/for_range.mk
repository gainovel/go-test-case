# @Author       : gainovel
# @Organization : Copyright © 2023-2024 gainovel.com All Rights Reserved.
# @Date         : 2024-03-14 19:41:38 星期四
# @ProductName  : GoLand
# @PrjectName   : test-case
# @File         : Makefiles/stdlib/runtime/for_range.mk
# @Version      : v0.1.0
# @Description  : 开发中···

# stdlib/runtime/for_range/features_and_usages/memory/001_test.go
TestName_2024_03_14_17_03_07/case1:
	go test -v -run TestName_2024_03_14_17_03_07/case1 github.com/gainovel/testcase/stdlib/runtime/for_range/features_and_usages/memory/
TestName_2024_03_14_17_03_07/share:
	go test -v -run TestName_2024_03_14_17_03_07/share github.com/gainovel/testcase/stdlib/runtime/for_range/features_and_usages/memory/
TestName_2024_03_14_17_03_07/string_to_bytes_or_runes:
	go test -v -run TestName_2024_03_14_17_03_07/string_to_bytes_or_runes github.com/gainovel/testcase/stdlib/runtime/for_range/features_and_usages/memory/

# stdlib/runtime/for_range/features_and_usages/memory/002_test.go
TestName_2024_03_14_22_45_35/add_elem_when_range:
	go test -v -run TestName_2024_03_14_22_45_35/add_elem_when_range github.com/gainovel/testcase/stdlib/runtime/for_range/features_and_usages/memory/
