```text
/**
 * @Author       : gainovel
 * @Organization : Copyright © 2023-2024 gainovel.com All Rights Reserved.
 * @Date         : 2024-03-11 14:33:42 星期一
 * @ProductName  : GoLand
 * @PrjectName   : test-case
 * @File         : docs/tests/stdlib/sync/map.md
 * @Version      : v0.1.0
 * @Description  : 开发中···
 **/
```

# Makefile test script

## stdlib/sync/map/features_usages_001_test.go


```shell
# all
make all -f Makefiles/stdlib/sync/map.mk

# 001/all
make 001/all -f Makefiles/stdlib/sync/map.mk

# testcases
make 001/sync.Map_1 -f Makefiles/stdlib/sync/map.mk
make 001/sync.Map.LoadOrStore -f Makefiles/stdlib/sync/map.mk
make 001/sync.Map.LoadAndDelete -f Makefiles/stdlib/sync/map.mk
```

