```text
/**
 * @Author       : gainovel
 * @Organization : Copyright © 2023-2024 gainovel.com All Rights Reserved.
 * @Date         : 2024-03-14 19:42:25 星期四
 * @ProductName  : GoLand
 * @PrjectName   : test-case
 * @File         : docs/tests/stdlib/runtime/for_range.md
 * @Version      : v0.1.0
 * @Description  : 开发中···
 **/
```

# go test script

## stdlib/runtime/for_range/features_and_usages/memory/001_test.go

```shell
make TestName_2024_03_14_17_03_07/case1 -f Makefiles/stdlib/runtime/for_range.mk
make TestName_2024_03_14_17_03_07/share -f Makefiles/stdlib/runtime/for_range.mk
make TestName_2024_03_14_17_03_07/string_to_bytes_or_runes -f Makefiles/stdlib/runtime/for_range.mk
```
## stdlib/runtime/for_range/features_and_usages/memory/002_test.go

```shell
make TestName_2024_03_14_22_45_35/add_elem_when_range -f Makefiles/stdlib/runtime/for_range.mk
```