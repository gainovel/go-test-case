```text
/**
 * @Author       : gainovel
 * @Organization : Copyright © 2023-2024 gainovel.com All Rights Reserved.
 * @Date         : 2024-03-11 13:15:31 星期一
 * @ProductName  : GoLand
 * @PrjectName   : test-case
 * @File         : docs/tests/stdlib/runtime/slice.md
 * @Version      : v0.1.0
 * @Description  : 开发中···
 **/
```

# Makefile test script

## stdlib/runtime/slice/features_usages_001_test.go

```shell
make 001/index_out_of_range -f Makefiles/stdlib/runtime/slice.mk
make 001/share_array -f Makefiles/stdlib/runtime/slice.mk
make 001/slice_copy -f Makefiles/stdlib/runtime/slice.mk
make 001/expansion_experiment -f Makefiles/stdlib/runtime/slice.mk
```


