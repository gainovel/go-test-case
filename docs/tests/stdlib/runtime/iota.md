```text
/**
 * @Author       : gainovel
 * @Organization : Copyright © 2023-2024 gainovel.com All Rights Reserved.
 * @Date         : 2024-03-11 14:16:12 星期一
 * @ProductName  : GoLand
 * @PrjectName   : test-case
 * @File         : docs/tests/stdlib/runtime/iota.md
 * @Version      : v0.1.0
 * @Description  : 开发中···
 **/
```

# Makefile test script

## stdlib/runtime/iota/features_usages_001_test.go


```shell
make 001/iota_1 -f Makefiles/stdlib/runtime/iota.mk
make 001/iota_2 -f Makefiles/stdlib/runtime/iota.mk
make 001/iota_3 -f Makefiles/stdlib/runtime/iota.mk
make 001/iota_4 -f Makefiles/stdlib/runtime/iota.mk
```

