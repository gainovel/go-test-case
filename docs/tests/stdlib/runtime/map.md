```text
/**
 * @Author       : gainovel
 * @Organization : Copyright © 2023-2024 gainovel.com All Rights Reserved.
 * @Date         : 2024-03-11 13:49:08 星期一
 * @ProductName  : GoLand
 * @PrjectName   : test-case
 * @File         : docs/tests/stdlib/runtime/map.md
 * @Version      : v0.1.0
 * @Description  : 开发中···
 **/
```

# Makefile test script

# stdlib/runtime/map/features_usages_001_test.go

```shell
make 001/concurrent_map_write -f Makefiles/stdlib/runtime/map.mk
make 001/assignment_to_entry_in_nil_map -f Makefiles/stdlib/runtime/map.mk
make 001/map_crud -f Makefiles/stdlib/runtime/map.mk
```


