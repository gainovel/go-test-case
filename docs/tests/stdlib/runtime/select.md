```text
/**
 * @Author       : gainovel
 * @Organization : Copyright © 2023-2024 gainovel.com All Rights Reserved.
 * @Date         : 2024-03-12 19:01:12 星期二
 * @ProductName  : GoLand
 * @PrjectName   : test-case
 * @File         : docs/tests/stdlib/runtime/select.md
 * @Version      : v0.1.0
 * @Description  : 开发中···
 **/
```

# go test script

## stdlib/runtime/select/features_usages_001_test.go

```shell
make TestName_2024_03_12_18_57_43/select_chan -f Makefiles/stdlib/runtime/select.mk
```

## stdlib/runtime/select/features_and_usages/comprehensive/001.go

```shell
make TestName_2024_03_13_10_44_15/case1 -f Makefiles/stdlib/runtime/select.mk
```
## stdlib/runtime/select/features_and_usages/memory/001_test.go
```shell
make TestName_2024_03_13_10_43_52/choose_one_case_to_run -f Makefiles/stdlib/runtime/select.mk
make TestName_2024_03_13_10_43_52/select_block_case1 -f Makefiles/stdlib/runtime/select.mk
make TestName_2024_03_13_10_43_52/select_block_case2 -f Makefiles/stdlib/runtime/select.mk
make TestName_2024_03_13_10_43_52/read_from_a_chan -f Makefiles/stdlib/runtime/select.mk
make TestName_2024_03_13_10_43_52/chan_returen_requirement -f Makefiles/stdlib/runtime/select.mk
make TestName_2024_03_13_10_43_52/default -f Makefiles/stdlib/runtime/select.mk
```

## stdlib/runtime/select/features_and_usages/memory/002_test.go

```shell
make TestName_2024_03_14_14_12_09/block_forever -f Makefiles/stdlib/runtime/select.mk
make TestName_2024_03_14_14_12_09/fail_fast -f Makefiles/stdlib/runtime/select.mk
make TestName_2024_03_14_14_12_09/wait_timeout -f Makefiles/stdlib/runtime/select.mk
```